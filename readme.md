# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

Práctica para comprender las diferentes cabeceras HTTP para control de cache.



# Instalación
-----------------------

```
$ make up
```


# Instrucciones
-----------------------

- Entra en `http://localhost` con el navegador Google Chrome
- Abre las DevTools, ve a la parte de `Networking`, filtra por imágenes
- Recarga la página mirando las cabeceras enviadas para cada tipo de imagen
- Recargando diferentes veces la página, haciendo un `Hard Refresh`, también
- Activa el throttling de ‘Good 3G’ y vuelve a probar la página


# Desinstalación
-----------------------

```
$ make down
```
